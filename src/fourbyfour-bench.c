/*
 * SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <stdbool.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>

#include "fourbyfour-bench.h"

struct impl_entry {
	const char *name;
	const struct fbfb_interface *(*get)(void);
};

static const struct impl_entry implementations[] = {
	{ "gold", fbfb_interface_get_gold },
	{ "gr", fbfb_interface_get_graphene },
	{ "noop", fbfb_interface_get_noop },
	{ "wm", fbfb_interface_get_weston },
};

static const struct fbfb_interface *
interface_from_name(const char *name)
{
	unsigned i;

	for (i = 0; i < ARRAY_LENGTH(implementations); i++) {
		if (strcmp(implementations[i].name, name) == 0)
			return implementations[i].get();
	}

	return NULL;
}

static void
print_help(const char *prog)
{
	unsigned i;
	const struct fbfb_interface *iface;

	printf(
		"4x4 matrix precision and performance benchmark tool.\n"
		"\n"
		"Usage: %s -h|-p|-s -m IMPL [options]\n"
		"where you must choose one of the operating modes:\n"
		"  -h, --help             Print this help text.\n"
		"  -p, --precision        Matrix inversion precision testing.\n"
		"  -s, --speed            Matrix inversion speed testing.\n"
		"and one method/implementation to be tested with -m, --method:\n",
		prog);

	for (i = 0; i < ARRAY_LENGTH(implementations); i++) {
		iface = implementations[i].get();
		printf("  -m %-10s          %s: %s\n",
		       implementations[i].name, iface->name, iface->desc);
	}

	printf(
		"Options in --precision mode may include:\n"
		"  -i, --implicit         Do not compute the inverse matrix explicitly,\n"
		"                         use a solver instead.\n"
		"  -o, --output FILE      Write results to a file named FILE.\n"
		"  -u, --quicktest        Very short run, less accurate results.\n"
		"  -v, --verbose          Increase verbosity level, up to twice.\n"
		"Options in --speed mode may include:\n"
		"  -u, --quicktest        Very short run, less accurate results.\n"
		);
}

static int
operation_help(const struct fbfb_options *opts)
{
	print_help(opts->prog);

	return 0;
}

static bool
fbfb_options_set_operation(struct fbfb_options *opts,
			   int (*op)(const struct fbfb_options *))
{
	if (opts->operation == NULL)
		opts->operation = op;

	if (opts->operation == op || opts->operation == operation_help)
		return true;

	fprintf(stderr, "Error: multiple operation modes given.\n");
	return false;
}

static bool
parse_options(struct fbfb_options *opts, int argc, char *argv[])
{
	static const struct option long_opts[] = {
		{ "help",              no_argument,       NULL, 'h' },
		{ "implicit",          no_argument,       NULL, 'i' },
		{ "method",            required_argument, NULL, 'm' },
		{ "output",            required_argument, NULL, 'o' },
		{ "precision",         no_argument,       NULL, 'p' },
		{ "quicktest",         no_argument,       NULL, 'u' },
		{ "speed",             no_argument,       NULL, 's' },
		{ "verbose",           no_argument,       NULL, 'v' },
		{ 0 }
	};
	static const char short_opts[] = "him:o:pusv";
	bool ret = true;
	int c;

	opts->prog = argv[0];
	opts->invtform = FBFB_INVERSE_TRANSFORM_EXPLICIT;

	while (1) {
		c = getopt_long(argc, argv, short_opts, long_opts, NULL);
		if (c == -1)
			break;

		switch (c) {
		case 'h':
			opts->operation = operation_help;
			break;
		case 'i':
			opts->invtform = FBFB_INVERSE_TRANSFORM_SOLVER;
			break;
		case 'm':
			if (opts->impl) {
				fprintf(stderr,
					"Error: -m option given multiple times.\n");
				ret = false;
			}
			opts->impl = interface_from_name(optarg);
			if (!opts->impl) {
				fprintf(stderr,
					"Error: '%s' is not a known method/implementation.\n",
					optarg);
				opts->impl = (void *)128;
				ret = false;
			}
			break;
		case 'o':
			if (opts->data_out_file) {
				fprintf(stderr,
					"Error: -o option given multiple times.\n");
				ret = false;
			}
			opts->data_out_file = optarg;
			break;
		case 'p':
			if (!fbfb_options_set_operation(opts, fbfb_operation_precision))
				ret = false;
			break;
		case 's':
			if (!fbfb_options_set_operation(opts, fbfb_operation_speed))
				ret = false;
			break;
		case 'u':
			opts->quick_test = true;
			break;
		case 'v':
			opts->verbose_level++;
			break;
		case '?':
			ret = false;
			break;
		default:
			fprintf(stderr, "Internal error in %s: char %d\n",
				__func__, c);
			ret = false;
			break;
		}
	}

	if (optind < argc) {
		fprintf(stderr, "Error: extra arguments given:");
		while (optind < argc)
			fprintf(stderr, " %s", argv[optind++]);
		fprintf(stderr, "\n");
		ret = false;
	}

	if (!opts->operation) {
		fprintf(stderr,
			"Error: operation was not specified, you need to pick one.\n");
		ret = false;
	}

	if (!opts->impl && opts->operation != operation_help) {
		fprintf(stderr,
			"Error: method/implementation was not specified, you need to pick one.\n");
		ret = false;
	}

	return ret;
}

int
main(int argc, char *argv[])
{
	struct fbfb_options opts = { 0 };

	if (!parse_options(&opts, argc, argv)) {
		print_help(argv[0]);
		return 1;
	}

	if (opts.operation == fbfb_operation_precision &&
	    opts.invtform == FBFB_INVERSE_TRANSFORM_SOLVER &&
	    opts.impl->solve == NULL) {
		fprintf(stderr,
			"Sorry, %s does not implement an inverse solver.\n",
			opts.impl->name);
		return 1;
	}

	return opts.operation(&opts);
}
