/*
 * SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <fourbyfour.h>

#include <cblas.h>
#include <lapacke.h>

#include "fourbyfour-bench.h"

static void
fbfb_gold_matrix_to_fbf(struct fbf_matrix *out, const union fbfb_matrix *in)
{
	*out = in->gold;
}

static void
fbfb_gold_matrix_from_fbf(union fbfb_matrix *out, const struct fbf_matrix *in)
{
	out->gold = *in;
}

static void
fbfb_gold_vector_to_fbf(struct fbf_vector *out,
			const union fbfb_vector *in)
{
	*out = in->gold;
}

static void
fbfb_gold_vector_from_fbf(union fbfb_vector *out, const struct fbf_vector *in)
{
	out->gold = *in;
}

static enum fbfb_inversion_result
fbfb_gold_invert(union fbfb_matrix *inv, const union fbfb_matrix *mat)
{
	struct fbf_matrix A = mat->gold;
	lapack_int ipiv[4];
	int ret;

	fbf_matrix_identity(&inv->gold);

	ret = LAPACKE_dgesv(LAPACK_COL_MAJOR, 4, 4,
			    A.v, 4, ipiv, inv->gold.v, 4);

	if (ret == 0)
		return FBFB_INVERSION_RESULT_OK;
	else
		return FBFB_INVERSION_RESULT_NOT_INVERTIBLE;
}

static void
fbfb_gold_transform(union fbfb_vector *y,
		    const union fbfb_matrix *A,
		    const union fbfb_vector *x)
{
	y->gold = (struct fbf_vector){{ 0, 0, 0, 0 }};
	cblas_dgemv(CblasColMajor, CblasNoTrans, 4, 4,
		    1.0, A->gold.v, 4, x->gold.v, 1, 0.0, y->gold.v, 1);
}

/*
static enum fbfb_inversion_result
fbfb_gold_decompose(union fbfb_decomposition *A_dec,
		    const union fbfb_matrix *mat)
{
	LAPACKE_dgetrf()
	LAPACKE_dgecon()
}

static void
fbfb_gold_solve(union fbfb_vector *x,
		const union fbfb_decomposition *A_dec,
		const union fbfb_vector *b)
{
	LAPACKE_dgetrs()
}
*/

static const struct fbfb_interface implementation_gold = {
	.name = "gold standard method",
	.desc = "BLAS and LAPACK, double precision",
	.precision = FBF_PRECISION_DOUBLE,
	.matrix_to_fbf = fbfb_gold_matrix_to_fbf,
	.matrix_from_fbf = fbfb_gold_matrix_from_fbf,
	.vector_to_fbf = fbfb_gold_vector_to_fbf,
	.vector_from_fbf = fbfb_gold_vector_from_fbf,
	.invert = fbfb_gold_invert,
	.transform = fbfb_gold_transform,
/*	.decompose = fbfb_gold_decompose,
	.solve = fbfb_gold_solve, */
};

const struct fbfb_interface *
fbfb_interface_get_gold(void)
{
	return &implementation_gold;
}
