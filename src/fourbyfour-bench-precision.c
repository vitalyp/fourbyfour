/*
 * SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <string.h>

#include <fourbyfour.h>

#include "fourbyfour-bench.h"

struct run_instance {
	const struct fbfb_interface *impl;

	enum fbf_matrix_rand_flags flags;
	double log2_cond;
	double abs_det;

	double rel_tolerance;
	bool verbose;
	enum fbfb_inverse_transform invtform;

	int count_ok; /* inversion succeeded and result is correct */
	int count_non_invertible; /* deemed non-invertible */
	int count_bad_result; /* inversion succeeded but result is incorrect */
	int count_conditioning_failure; /* Cannot create input data */

	double max_reldiff;

	FILE *logfile;
};

static enum fbfb_inversion_result
explicit_inversion(struct fbf_matrix *Itest,
		   const union fbfb_matrix *impl_mat /* R converted */,
		   const struct fbf_matrix *R,
		   const struct fbfb_interface *impl)
{
	enum fbfb_inversion_result ret;
	struct fbf_matrix Rinv;
	union fbfb_matrix impl_inv;

	ret = impl->invert(&impl_inv, impl_mat);
	if (ret == FBFB_INVERSION_RESULT_NOT_INVERTIBLE)
		return ret;

	impl->matrix_to_fbf(&Rinv, &impl_inv);
	fbf_matrix_matrix_mul(Itest, &Rinv, R);

	return ret;
}

static enum fbfb_inversion_result
solver_inversion(struct fbf_matrix *Itest,
		 const union fbfb_matrix *impl_mat /* R converted */,
		 const struct fbf_matrix *R,
		 const struct fbfb_interface *impl)
{
	enum fbfb_inversion_result ret;
	union fbfb_decomposition A_dec;
	struct fbf_vector vec;
	union fbfb_vector impl_vec;
	union fbfb_vector result;
	int i;

	ret = impl->decompose(&A_dec, impl_mat);
	if (ret == FBFB_INVERSION_RESULT_NOT_INVERTIBLE)
		return ret;

	/*
	 * Inverse-transform each column of the original matrix,
	 * effectively computing Itest = R^-1 * R.
	 */
	for (i = 0; i < 4; i++) {
		fbf_matrix_column_get(&vec, R, i);
		impl->vector_from_fbf(&impl_vec, &vec);
		impl->solve(&result, &A_dec, &impl_vec);
		impl->vector_to_fbf(&vec, &result);
		fbf_matrix_column_set(Itest, &vec, i);
	}

	return ret;
}

static void
run_instance_once(struct run_instance *inst)
{
	struct fbf_matrix R;
	struct fbf_matrix I;
	struct fbf_matrix Itest = { { 0 } };
	struct fbf_matrix Rtrunc;
	union fbfb_matrix impl_mat;
	enum fbfb_inversion_result ret = FBFB_INVERSION_RESULT_NOT_INVERTIBLE;
	struct fbf_matrix_conditions mc;
	double reldiff;

	fbf_matrix_rand(&R, inst->flags,
			exp2(inst->log2_cond), inst->abs_det);
	inst->impl->matrix_from_fbf(&impl_mat, &R);

	inst->impl->matrix_to_fbf(&Rtrunc, &impl_mat);
	fbf_matrix_cond_analyse(&mc, &Rtrunc, inst->impl->precision);

	if (inst->verbose) {
		printf("Implementation matrix log2(cond) = %g, rank %d\n",
		       mc.log2_cond_number, mc.rank);
	}

	if (fabs(mc.log2_cond_number - inst->log2_cond) > 0.5) {
		inst->count_conditioning_failure++;
		return;
	}

	switch (inst->invtform) {
	case FBFB_INVERSE_TRANSFORM_EXPLICIT:
		ret = explicit_inversion(&Itest, &impl_mat, &R, inst->impl);
		break;
	case FBFB_INVERSE_TRANSFORM_SOLVER:
		ret = solver_inversion(&Itest, &impl_mat, &R, inst->impl);
		break;
	}

	if (ret == FBFB_INVERSION_RESULT_NOT_INVERTIBLE) {
		inst->count_non_invertible++;
		return;
	}

	fbf_matrix_identity(&I);
	reldiff = fbf_matrix_rel_error_inf(&Itest, &I);

	if (inst->max_reldiff < reldiff)
		inst->max_reldiff = reldiff;

	if (inst->logfile)
		fprintf(inst->logfile, "%.16g\n", reldiff);

	if (reldiff < inst->rel_tolerance) {
		inst->count_ok++;
		return;
	}

	inst->count_bad_result++;

	if (inst->verbose) {
		printf("Bad inversion result, reldiff %g:\n", reldiff);
	}
}

static void
run_instance_print(const struct run_instance *inst)
{
	double digits = 0.0;

	switch (inst->impl->precision) {
	case FBF_PRECISION_DOUBLE:
		digits = 53.0 / log2(10.0);
		break;
	case FBF_PRECISION_SINGLE:
		digits = 24.0 / log2(10.0);
		break;
	}

	printf("Testing %s (FP precision %.1f digits) with random matrices having:\n",
		inst->impl->name, digits);

	if (inst->flags == FBF_MATRIX_RAND_FLAG_NONE)
		printf("- unrestricted [-1.0, 1.0] elements\n");

	if (inst->flags & FBF_MATRIX_RAND_FLAG_COND) {
		printf("- 2-norm condition number %g (lose up to %.1f digits of precision)\n",
			exp2(inst->log2_cond), inst->log2_cond / log2(10.0));
	}

	if (inst->flags & FBF_MATRIX_RAND_FLAG_ABSDET)
		printf("- absolute value of determinant %g\n", inst->abs_det);

	printf("Results from %d iterations using %s:\n",
		inst->count_ok +
		inst->count_non_invertible +
		inst->count_bad_result,
		(inst->invtform == FBFB_INVERSE_TRANSFORM_EXPLICIT) ?
			"explicit inverse matrices" : "inverse solvers");

	printf( "%8d acceptable inversions\n"
		"%8d determined non-invertible\n"
		"%8d inversions with too high error (need %.1f correct digits)\n"
		"%8d input data conditioning failures\n",
		inst->count_ok,
		inst->count_non_invertible,
		inst->count_bad_result, -log10(inst->rel_tolerance),
		inst->count_conditioning_failure);

	if (inst->count_ok + inst->count_bad_result > 0) {
		printf("    Max relative error %g (%.1f correct digits)\n",
			inst->max_reldiff, -log10(inst->max_reldiff));
	}
}

static bool
iterated_run(double log2_cond,
	     FILE *resultfile,
	     const struct fbfb_options *opts)
{
	struct run_instance inst = { 0 };
	int repeats;
	int i;

	inst.impl = opts->impl;
	inst.flags = FBF_MATRIX_RAND_FLAG_COND;
	inst.log2_cond = log2_cond;
	inst.abs_det = 10.0;
//	inst.logfile = fopen("reldiff.txt", "w");
	inst.verbose = opts->verbose_level > 1;
	inst.invtform = opts->invtform;

	/* Require five correct digits, roughly. */
	inst.rel_tolerance = 1e-5;

	if (opts->quick_test)
		repeats = 10;
	else
		repeats = 1000;

	/*
	 * The sequence of random matrices should be the same on every run
	 * with the same parameters.
	 */
	srandom(13);

	inst.max_reldiff = -1.0;
	for (i = 0; i < repeats; i++)
		run_instance_once(&inst);

	if (opts->verbose_level > 0)
		run_instance_print(&inst);

	if (inst.logfile)
		fclose(inst.logfile);

	if (inst.count_conditioning_failure > 0)
		return false;

	/* log2(cond), n_ok, n_noninv, n_bad, -log2(max_reldiff) */
	if (resultfile) {
		double corr_digits = -log2(inst.max_reldiff);

		fprintf(resultfile, "%.3g %d %d %d %.3g\n",
			inst.log2_cond,
			inst.count_ok,
			inst.count_non_invertible,
			inst.count_bad_result,
			corr_digits > 0.0 ? corr_digits : 0.0);
	}

	return true;
}

static void
verify_implementation(const struct fbfb_options *opts)
{
	const struct fbfb_interface *impl = opts->impl;
	struct fbf_matrix A;
	struct fbf_matrix A_ref;
	struct fbf_vector x;
	struct fbf_vector y;
	struct fbf_vector y_ref;
	union fbfb_matrix A_impl;
	union fbfb_vector x_impl;
	union fbfb_vector y_impl;

	if (opts->verbose_level > 1)
		printf("Verifying interface correctness...\n");

	/*
	 * Small integer numbers should come out correct exactly,
	 * regardless of implementation precision.
	 */
	A_ref = FBF_MATRIX_INIT_VISUAL(
		2,   0, 0,  7,
		1,   3, 0,  0,
		0,   0, 1, -5,
		0, -11, 0,  4
	);

	x = (struct fbf_vector){{ 1, 2, 3, 4 }};

	/* y_ref = A_ref * x */
	y_ref = (struct fbf_vector){{ 30, 7, -17, -6 }};

	/* Test matrix conversion roundtrip */
	impl->matrix_from_fbf(&A_impl, &A_ref);
	impl->matrix_to_fbf(&A, &A_impl);
	fbf_assert_eq_matrix_releps_inf(&A, &A_ref, 0);
	if (opts->verbose_level > 1)
		printf("    matrix conversion: OK\n");

	/* Test vector conversion roundtrip. */
	impl->vector_from_fbf(&x_impl, &x);
	impl->vector_to_fbf(&y, &x_impl);
	fbf_assert_eq_vector_releps(&y, &x, 0);
	if (opts->verbose_level > 1)
		printf("    vector conversion: OK\n");

	/* Test matrix-vector product. */
	impl->transform(&y_impl, &A_impl, &x_impl);
	impl->vector_to_fbf(&y, &y_impl);
	fbf_assert_eq_vector_releps(&y, &y_ref, 0);
	if (opts->verbose_level > 1)
		printf("    matrix-vector product: OK\n");
}

int
fbfb_operation_precision(const struct fbfb_options *opts)
{
	FILE *resultfile = stdout;
	int i;

	if (strcmp(opts->impl->name, "no-op") == 0) {
		fprintf(stderr, "Error: the no-op implementation does not support precision testing.\n");
		return 1;
	}

	verify_implementation(opts);

	if (opts->data_out_file) {
		resultfile = fopen(opts->data_out_file, "w");
		if (!resultfile) {
			fprintf(stderr,
				"Error opening file '%s' for writing: %s\n",
				opts->data_out_file, strerror(errno));
			return 1;
		}
	}

	for (i = 1; i < 100; i++)
		if (!iterated_run(i, resultfile, opts))
			break;

	if (opts->data_out_file &&
	    fclose(resultfile) != 0) {
		fprintf(stderr,
			"Error writing to file '%s': %s\n",
			opts->data_out_file, strerror(errno));
		return 1;
	}

	return 0;
}
