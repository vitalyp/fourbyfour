/*
 * SPDX-FileCopyrightText: 2020 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#pragma once

/* This file does not come from Weston */

#if !defined(WESTONMATRIX_STATIC_LIB) && defined(__GNUC__) && __GNUC__ >= 4
#define WL_EXPORT __attribute__ ((visibility("default")))
#else
#define WL_EXPORT
#endif
