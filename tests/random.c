/*
 * SPDX-FileCopyrightText: 2020 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <fourbyfour.h>

/** Cumulative Distribution Function for the uniform [0, 1] distribution */
static double
cdf(double x)
{
	if (x < 0.0)
		return 0.0;
	if (x > 1.0)
		return 1.0;
	return x;
}

/* Comparison for qsort() */
static int
cmp_double(const void *pa, const void *pb)
{
	double a = *(double *)pa;
	double b = *(double *)pb;

	if (a < b)
		return -1;
	if (a > b)
		return 1;
	if (a == b)
		return 0;

	abort();
}

/*
 * Test if the random generator is sufficiently uniform.
 */
int
main(void)
{
	double *R;
	const int N = 100000;
	double Dn = -1.0;
	double Di;
	double Fx;
	double x;
	int i;

	srandom(13);

	R = calloc(N, sizeof(double));

	/* Generate the random sample */
	for (i = 0; i < N; i++) {
		R[i] = fbf_rand_zero_one();

		if (i < 10)
			printf("random number %g\n", R[i]);
	}

	/*
	 * Sort it to form the empirical distribution function F(x):
	 * F(x) = (i + 1) / N, x = R[i], i = 0 .. N-1
	 *
	 * F(x) * N = how many observations are <= x
	 */
	qsort(R, N, sizeof(double), cmp_double);

	printf("Zero-one random test, min %g, max %g\n", R[0], R[N - 1]);

	if (R[0] < 0.0 || R[N - 1] > 1.0) {
		fprintf(stderr, "Zero-one random test failed: range\n");
		exit(1);
	}

	/* Dn = sup_x | F(x) - cdf(x) | */
	for (i = 0; i < N; i++) {
		x = R[i];
		Fx = (i + 1.0) / N;
		Di = fabs(Fx - cdf(x));
		if (Di > Dn)
			Dn = Di;
	}

	printf("N=%d, Kolmogorov-Smirnov statistic Dn = %g\n", N, Dn);

	/*
	 * This is not the proper statistical test. However, it is
	 * hopefully enough to rule out gross errors in the random number
	 * generation.
	 *
	 * http://www.ciphersbyritter.com/JAVASCRP/NORMCHIK.HTM seems to
	 * say that for 100k observations, Prob(Dn <= 0.0034) = 90%.
	 */
	if (Dn > 0.0034) {
		fprintf(stderr, "Random test failed.\n");
		exit(1);
	}

	free(R);
	return 0;
}
