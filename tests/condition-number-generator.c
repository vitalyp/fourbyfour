/*
 * SPDX-FileCopyrightText: 2020 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <fourbyfour.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>

/*
 * Test that the random matrices obey the restrictions.
 */
static void
test_matrix_rand_once(uint32_t flags,
		      double cond,
		      double abs_det,
		      bool print_mc)
{
	struct fbf_matrix M;
	struct fbf_matrix_conditions mc;

	fbf_matrix_rand(&M, flags, cond, abs_det);

	fbf_matrix_cond_analyse(&mc, &M, FBF_PRECISION_DOUBLE);

	if (print_mc) {
		fbf_matrix_print(stdout, &M);
		fbf_matrix_conditions_print(stdout, &mc);
	}

	if (flags & FBF_MATRIX_RAND_FLAG_COND) {
		if (isinf(cond)) {
			/* double precision has 53 bits significand */
			if (mc.log2_cond_number < 52) {
				fprintf(stderr,
					"Assertion failure, expected %f >= 52.\n",
					mc.log2_cond_number);
				exit(1);
			}
		} else {
			fbf_assert_eq_double_releps(mc.log2_cond_number,
						    log2(cond), 1e-6);
		}
	}

	if (flags & FBF_MATRIX_RAND_FLAG_ABSDET) {
		if (abs_det == 0.0) {
			if (mc.abs_det > 1e-14) {
				fprintf(stderr,
					"Assertion failure, expected %f <= 1e-14.\n",
					mc.abs_det);
				exit(1);
			}
		} else {
			fbf_assert_eq_double_releps(mc.abs_det, abs_det, 1e-6);
		}
	}
}

static void
test_matrix_rand(uint32_t flags, double cond, double abs_det, int N)
{
	int i;

	srandom(13);

	printf("fbf_matrix_rand");
	if (flags & FBF_MATRIX_RAND_FLAG_COND)
		printf(" cond = %f (log2(cond) = %f)", cond, log2(cond));
	if (flags & FBF_MATRIX_RAND_FLAG_ABSDET)
		printf(" abs_det = %f", abs_det);
	printf("\n");

	for (i = 0; i < N; i++)
		test_matrix_rand_once(flags, cond, abs_det, i < 5);
}

int
main(void)
{
	test_matrix_rand(FBF_MATRIX_RAND_FLAG_COND, 1.1, 0.0, 100);
	test_matrix_rand(FBF_MATRIX_RAND_FLAG_COND, 10.0, 0.0, 100);
	test_matrix_rand(FBF_MATRIX_RAND_FLAG_COND, 1e6, 0.0, 100);
	test_matrix_rand(FBF_MATRIX_RAND_FLAG_COND, INFINITY, 0.0, 100);

	test_matrix_rand(FBF_MATRIX_RAND_FLAG_ABSDET, 0.0, 15.0, 100);
	test_matrix_rand(FBF_MATRIX_RAND_FLAG_ABSDET, 0.0, 1e-6, 100);
	test_matrix_rand(FBF_MATRIX_RAND_FLAG_ABSDET, 0.0, 0.0, 100);

	test_matrix_rand(FBF_MATRIX_RAND_FLAG_COND | FBF_MATRIX_RAND_FLAG_ABSDET,
			 100.0, 15.0, 100);
	test_matrix_rand(FBF_MATRIX_RAND_FLAG_COND | FBF_MATRIX_RAND_FLAG_ABSDET,
			 1e6, 2.0, 100);

	return 0;
}
