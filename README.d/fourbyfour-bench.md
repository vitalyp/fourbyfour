---
SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
SPDX-License-Identifier: CC-BY-4.0
---

Table of Contents

[[_TOC_]]

[Front page](../README.md)

# Command `fourbyfour-bench`

```
$ ./build/src/fourbyfour-bench --help
4x4 matrix precision and performance benchmark tool.

Usage: ./build/src/fourbyfour-bench -h|-p|-s -m IMPL [options]
where you must choose one of the operating modes:
  -h, --help             Print this help text.
  -p, --precision        Matrix inversion precision testing.
  -s, --speed            Matrix inversion speed testing.
and one method/implementation to be tested with -m, --method:
  -m gold                gold standard method: BLAS and LAPACK, double precision
  -m gr                  graphene: a math library for 2D and 3D graphics
  -m noop                no-op: used to measure overhead in speed testing
  -m wm                  weston-matrix: the matrix implementation from Weston
Options in --precision mode may include:
  -i, --implicit         Do not compute the inverse matrix explicitly,
                         use a solver instead.
  -o, --output FILE      Write results to a file named FILE.
  -u, --quicktest        Very short run, less accurate results.
  -v, --verbose          Increase verbosity level, up to twice.
Options in --speed mode may include:
  -u, --quicktest        Very short run, less accurate results.
```

## Available inversion implementations

### BLAS and LAPACK `-m gold`

This is a straightforward implementation using BLAS and LAPACK in double
precision, hence named the gold standard method. It is not optimised for
speed, and implements only the explicit inverse matrix computation. It uses
LAPACK function DGESV for computing the inverse, and BLAS function DGEMV
for computing the matrix-vector multiplication.

### Graphene `-m gr`

This uses the [Graphene](http://ebassi.github.io/graphene/) explicit inverse
matrix function `graphene_matrix_inverse()` and matrix-vector multiplication
function `graphene_matrix_transform_vec4()`. It is single precision.

### No-op `-m noop`

The no-op implementation does not compute anything. Instead, it is used for
measuring the speed measurement overhead in the framework. The overhead
should be taken into account, when evaluating the speed of real
implementations.

### Weston-matrix `-m wm`

This is the `weston_matrix` implementation lifted from
[Weston](https://gitlab.freedesktop.org/wayland/weston) 9.0.0. The
implementation uses single precision in its public API, but computes in
double precision. The public API exposes only explicit inverse matrix
computation via `weston_matrix_invert()`, and `weston_matrix_transform()`
for matrix-vector multiplication.

The implementation here exposes also the private functions `matrix_invert()`
and `inverse_transform()`. The former computes an intermediate inverse
representation (LU-decomposition and permutation vector) in double precision,
and the latter uses that to inverse-transform a vector. This provides an
example of the "solver" type.


## Speed test

```
$ ./build/src/fourbyfour-bench --speed -m noop
Warming up for 1 seconds...
Measuring explicit matrix inversion for 5 seconds...
   done in 5.000 seconds, 3228918399 iterations, average 1.55 ns per invert() call.
Measuring matrix-vector multiplication for 5 seconds...
   done in 5.000 seconds, 3228125270 iterations, average 1.55 ns per transform() call.
Measuring decomposition for inversion for 5 seconds...
   done in 5.000 seconds, 3233627987 iterations, average 1.55 ns per decompose() call.
Measuring inverse-transformation solver for 5 seconds...
   done in 5.000 seconds, 3236829976 iterations, average 1.54 ns per solve() call.

Speed test results for no-op:

            preparing   transforming          sum
          the inverse       a vector
explicit      1.55 ns        1.55 ns       3.1 ns
solver        1.55 ns        1.54 ns      3.09 ns
```

## Precision test

Precision testing uses the explicit matrix inversion by default, but when
available one can use `--implicit` option to use the solver interface
instead. When using the solver interface, the framework does not compute
an explicit inverse matrix but inverse-transforms the columns of the
input (random) matrix producing the matrix $`\boldsymbol{I}'`$ directly
(see [Precision Testing Method](precision_testing.md)).

Precision testing iterates through condition numbers
```math
C_i = 2^i \quad \mathrm{for}\quad i = 1, 2, 3, ...
```
This roughly means that with $`C_i`$ as the matrix condition number, the
inverse matrix should lose $`i`$ bits of precision.

For each $`C_i`$, the program generates 1000 pseudorandom matrices
$`\boldsymbol{R}`$ with condition number $`C_i`$. Iterating over $`C_i`$
stops when any random matrix fails the *conditioning test*. As different
matrix inversion implementations use different precision in their API,
converting $`\boldsymbol{R}`$ to the implementation API type may lose
precision. As a result, the condition number of the converted matrix may
differ. In the conditioning test, $`\boldsymbol{R}`$ is converted to the
implementation API type and then straight back to the framework type, and
it's condition number is computed again. If this new condition number differs
from $`C_i`$ by more than 0.5, the conditioning test has failed and the
results for $`C_i`$ are discarded. This provides a natural stopping condition
on how close to singular matrices are feasible to be used in testing an
implementation.

For each $`C_i`$, the results contain the counts of successful matrix
inversions, bad matrix inversions, and failed matrix inversions, totalling to
1000. A failed matrix inversion is one where the implementation refused to
produce a result. The results also contain the number of correct bits for the
worst error in the 1000 random matrix inversions.

The program has a hardcoded precision threshold of five correct digits. This
is explicitly plotted on the graphs to show where the test program considers
an inversion to have a bad result. A bad result is when the precision falls
below the threshold. Five digits was chosen because it is the precision that
a display server would require at minimum when dealing with matrices
addressing pixels at modern desktop resolutions.

The program produces a text file with the results which can be visualised
with the Gnuplot script
[plot_precision_graph.gp](../gnuplot/plot_precision_graph.gp).

```
$ ./build/src/fourbyfour-bench --precision -m wm -o wm-explicit.txt
$ ./gnuplot/plot_precision_graph.gp wm-explicit.txt
```

The above produces a file `wm-explicit.txt.svg`. Here is an example of what
such graph could look like.

![precision graph](README.d/precision_plot.svg "weston-matrix inversion behavior")

Here one can see that the inversion method works fine up to condition number
$`2^7`$ but then starts suffering from precision loss greater than the
threshold of five digits. The API of the implementation used here uses single
precision floating point which has 24 bits of precision. Five digits is
roughly 17 bits. If you start with 24 bits of precision, then lose 7 bits,
you are left with 17 bits. The graph shows that the implementation follows
this theory quite well. It also shows that the conditioning test starts
failing after $`2^{23}`$.
