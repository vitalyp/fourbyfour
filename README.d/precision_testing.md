---
SPDX-FileCopyrightText: 2021 Pekka Paalanen <pq@iki.fi>
SPDX-License-Identifier: CC-BY-4.0
---

Table of Contents

[[_TOC_]]

[Front page](../README.md)

# Precision Testing Method

## Introduction

When evaluating 4x4 matrix inversion algorithms one naturally wants the
results to be correct. Floating-point arithmetic is imprecise which means
that testing for correctness is not quite straightforward. An error measure
is needed and some justification on where to put the threshold of acceptance.

Not all matrices are invertible. Non-invertible matrices are called singular.
Given the imprecision of floating-point arithmetic, a matrix that is
mathematically invertible could be so poorly conditioned numerically that
a matrix inversion algorithm fails to produce a result or produces a result
that is wildly inaccurate.

To probe the behavior of an inversion algorithm with near-singular matrices
requires generating near-singular random matrices. This is not trivial.
If one generates random matrices where each element has a uniform
distribution and is independent and identically distributed (i.i.d), the
chances of getting a singular or a near-singular matrix is extremely low.
Such a matrix needs to be further manipulated to make it near-singular.

One measure of how singular a matrix is is its **condition number**. There is
not a single definition of a condition number but it depends on the chosen
matrix norm. See

- [How to Measure Errors] in the Netlib LAPACK documentation
- [What is the Condition Number of a Matrix?](https://blogs.mathworks.com/cleve/2017/07/17/what-is-the-condition-number-of-a-matrix/)
- Wolfram Mathworld [Condition Number](https://mathworld.wolfram.com/ConditionNumber.html)

However, it must especially noted that while **determinant** being zero is
mathematically equivalent of a matrix being singular, the value of the
determinant is *not* a good measure of how close to singular a matrix is.
See:

- [Why is det a bad way to check matrix singularity?](https://www.mathworks.com/matlabcentral/answers/400327-why-is-det-a-bad-way-to-check-matrix-singularity)
- [Matrix Determinant, Limitations](https://www.mathworks.com/help/matlab/ref/det.html#bubi9tw)
- [Golub Matrices, Deceptively Ill Conditioned](https://blogs.mathworks.com/cleve/2015/08/24/golub-matrices-deceptively-ill-conditioned/)

If you take any N×N matrix $`\boldsymbol{A}`$, and multiply it with a scalar
$`k`$, then $`\det(k\boldsymbol{A}) = k^N \det(\boldsymbol{A})`$.
If the determinant was not exactly zero, you can choose $`k`$ to get any
determinant value you want. Multiplying a matrix with a non-zero scalar
does not affect its level of singularity or condition number. Therefore
the value of the determinant is largely unrelated to the difficulty of
inverting the matrix.

## Inversion Error

The program uses a residual approach for measuring inversion error.
It starts with a random 4x4 matrix $`\boldsymbol{R}`$, computes the numerical
inverse $`\boldsymbol{R}^{-1}`$ with the algorithm to be tested, and then
computes
```math
\boldsymbol{I}' = \boldsymbol{R}^{-1} \boldsymbol{R} .
```
Mathematically $`\boldsymbol{I}'`$ should equal the identity matrix
$`\boldsymbol{I}`$ if $`\boldsymbol{R}`$ is not strictly singular. For
numerical reasons, the result is rarely exact. For a good matrix inversion
implementation the result should be closer to exact the further away from
singular $`\boldsymbol{R}`$ is, roughly speaking. Conversely, even a good
numerical algorithm will produce increasing errors or refuse to produce a
result when $`\boldsymbol{R}`$ goes towards singular.

To get a single number for representing the inversion accuracy, one must
measure how close $`\boldsymbol{I}'`$ is to $`\boldsymbol{I}`$. Here
the relative error $`r`$ with infinity norm has been chosen,
```math
r = \frac{\|\boldsymbol{I}'-\boldsymbol{I}\|_\infty}{\|\boldsymbol{I}\|_\infty} .
```
See [How to Measure Errors] for a discussion on error measuring and matrix
norms. Infinity norm is defined as the maximum over rows of the sum of the
row's absolute values,
```math
\|\boldsymbol{A}\|_\infty = \max_i \sum_j |a_{i,j}| ,
```
and so $`\|\boldsymbol{I}\|_\infty = 1`$, leaving
```math
r = \|\boldsymbol{I}'-\boldsymbol{I}\|_\infty .
```

When comparing to the identity matrix, relative error becomes equivalent to
absolute error. However, with the concept of relative error we can use $`r`$
to say how many digits of precision $`\boldsymbol{I}'`$ has. The precision is
$`\log_{10} r`$ digits or $`\log_2 r`$ bits.

## Generating Near-Singular Matrices

The precision benchmark program generates random matrices with a
pre-determined condition number to test an inversion algorithm's resilience
towards near-singular matrices. Generating the matrices works as follows.

First, a full matrix $`\boldsymbol{R}`$ with i.i.d random elements is created
with the pseudorandom number generator. The distribution is the uniform
distribution on the range $`[-1, 1]`$.

The singular value decomposition is computed which produces the three
matrices
```math
\boldsymbol{U}\boldsymbol{S}\boldsymbol{V}^\mathrm{T} = \boldsymbol{R} .
```
Matrix $`\boldsymbol{S}`$ is diagonal
```math
\boldsymbol{S} =
\begin{bmatrix}
	\sigma_0 & 0 & 0 & 0 \\
	0 & \sigma_1 & 0 & 0 \\
	0 & 0 & \sigma_2 & 0 \\
	0 & 0 & 0 & \sigma_3
\end{bmatrix}
```
containing the singular values $`\sigma_i`$ of $`\boldsymbol{R}`$ from
largest to smallest. The 2-norm condition number of $`\boldsymbol{R}`$
is $`\sigma_0 / \sigma_3`$. Also,
$`|\det(\boldsymbol{R})| = \prod_{i = 0}^3 \sigma_i`$, but that
is irrelevant here.

Program generates the diagonal matrix $`\boldsymbol{S}'`$ by
```math
\begin{aligned}
\sigma_0' &= \sigma_0 \\
\sigma_i' &= e^{\frac{-i \log C}{3}} \sigma_0 \quad \mathrm{for}\quad i = 1, 2, 3
\end{aligned}
```
where $`C`$ is the desired 2-norm condition number. Obviously,
$`\sigma_0' / \sigma_3' = C`$. The singular values are exponentially
spaced.

Then, the new matrix is assembled as
```math
\boldsymbol{R}' = \boldsymbol{U}\boldsymbol{S}'\boldsymbol{V}^\mathrm{T} .
```
The random matrix $`\boldsymbol{R}'`$ now has the condition number $`C`$.

It is also possible to simultaneously set the absolute value of the
determinant to any desired non-zero value. This is implemented in the
program, but not used.


[How to Measure Errors]: http://www.netlib.org/lapack/lug/node75.html
