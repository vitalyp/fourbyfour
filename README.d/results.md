---
SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
SPDX-License-Identifier: CC-BY-4.0
---

Table of Contents

[[_TOC_]]

[Front page](../README.md)

# Benchmark Results

These example results were gathered with a AMD Ryzen 5 2400G processor
on Feb 14th, 2021. The build configuration was:
```
$ meson build -Ddebug=false -Doptimization=3 -Dc_args=-march=native
```

## Speed results

Results obtained with:
```
$ ./src/fourbyfour-bench --speed -m noop
$ ./src/fourbyfour-bench --speed -m gr
$ ./src/fourbyfour-bench --speed -m wm
```

| Method        | type     | preparing<br>the inverse | transforming<br>a vector | sum |
|---------------|----------|--------:|--------:|--------:|
| no-op         | explicit | 1.55 ns | 1.55 ns | 3.09 ns |
| no-op         | solver   | 1.55 ns | 1.55 ns | 3.09 ns |
| Graphene      | explicit | 10.8 ns | 2.58 ns | 13.3 ns |
| Weston matrix | explicit | 89.1 ns | 2.84 ns |   92 ns |
| Weston matrix | solver   | 60.3 ns | 15.9 ns | 76.2 ns |

The no-op method only measures the overhead of measuring execution time
through the framework, it does not actually compute anything. One should
subtract the overhead from the other results.

The "solver" type computes some kind of representation of the inverse matrix
without explicitly computing the inverse matrix. This representation still
allows inverse-transforming vectors. If an explicit inverse matrix is
needed, in theory it would take one solver preparation cycle and four
inverse-transforms of the unit vectors of the identity matrix.

The Weston matrix results for explicit vs. solver somehow has the explicit
inversion computed faster than what the solver results would imply, given
the implementation of explicit is exactly one solver prepration plus
four inverse-transforms.

If you are to compute the inverse-transform of one vector and then throw away
the matrix inverse, the solver approach seems significantly faster, assuming
the speed difference observed with Weston matrix could be transferred to
Graphene implementation.

## Precision results

For the description of each method, see
[`fourbyfour-bench`](fourbyfour-bench.md).

### BLAS and LAPACK (gold standard)

```
$ ./build/src/fourbyfour-bench --precision -m gold
```

![precision graph: gold](result-gold.svg "LAPACK inversion behavior")

This is an example of a double precision floating point implementation.
Double has 53 bits of precision initially. The first failures to reach the
five correct digits threshold are seen at around 30-31 bits of precision lost
to the input matrix being nearly singular. Around 40 bits of loss, almost
none of the test inputs produce enough precision on the inverse. As five
digits is 16-17 bits, this seems reasonable, but maybe one could be more
smart with BLAS and LAPACK API usage to gain a little more.

LAPACK does offer tools to implement detection of non-invertible matrices,
but that requires making a decision on where to put the threshold. Hence,
this was not implemented in `fourbyfour-bench`.

### Graphene

```
$ ./build/src/fourbyfour-bench --precision -m gr
```

![precision graph: Graphene](result-graphene.svg "Graphene inversion behavior")

Graphene uses single precision floating point. Single has 24 bits of
precision to start with. The first failures to reach five correct digits
start at condition number $`2^6`$, or 6 bits of expected precision loss due
to input matrices being close to singular.

Graphene is the only implementation that actually detects non-invertible
matrices which can be seen in the rise of the "non-invertible" curve.
Looking at the curves though, it seems the non-invertible detection kicks in
when the inverted matrices have only maybe 6 bits of precision left in the
worst case of the random input data set. That is barely two digits.

That statement has to be taken with caution though, because the random input
matrix determinant values were not controlled in this test. If graphene uses
determinant value to detect non-invertible matrices, it would theoretically
be possible to construct the input data set to produce any arbitrary
non-invertible detection result.

### weston-matrix (explicit matrix inversion)

```
$ ./build/src/fourbyfour-bench --precision -m wm
```

![precision graph: weston-matrix explicit](result-wm-e.svg "weston-matrix explicit inversion behavior")

The weston-matrix public API offers single precision explicit matrix
inversion. The first failures to reach five correct digits start at
condition number $`2^8`$, which is a couple bits later than with Graphene.

The inverse precision degradation when input matrices become closer to
singular seems to be somewhat less than with Graphene which can be seen
from the "min precision" curve. This curve plots the worst case inverse's
precision on the input data set. Note, that given the pseudo-random
number generator is always initialized the same way and the input data set
is also always generated the same way, all implementations should be
working on identical input data sets.

The weston-matrix implementation does attempt to detect non-invertible
matrices, but as shown, it never triggers. It is implemented as checking
the magniture of the pivot element during LU-decomposition, which has been
mentioned as not a good method either (citation needed, I recall reading it
somewhere but forgot where).

### weston-matrix (solver)

```
$ ./build/src/fourbyfour-bench --precision -m wm -i
```

![precision graph: weston-matrix solver](result-wm-i.svg "weston-matrix solver inverse-transform behavior")

The weston-matrix solver method is kind of cheating. Even though the matrix
and vector API is still single precision, the representation of a matrix
inverse is double precision. Furthermore, as the explicit inverse matrix is
never truncated into single precision (it is never even computed, in fact),
this method is basically using double precision. On the graph, this is quite
obvious.

Because of the conditioning test in `fourbyfour-bench`, the testing never
reaches the level of singular input matrices to show any errors. The "min
precision" curve does look quite linear, so presumably one could extrapolate
it. At condition number $`2^{22}`$, the worst case remaining precision is
around 32-33 bits. Surprisingly that seems much better than the LAPACK method
at $`2^{22}`$, suggesting that maybe LAPACK API is not used quite right in
`fourbyfour-bench`, or maybe compiler optimisation in weston-matrix do
wonders (e.g. using FPU instructions that deliver more precision than double).
