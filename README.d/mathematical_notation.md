---
SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
SPDX-License-Identifier: CC-BY-4.0
---

Table of Contents

[[_TOC_]]

[Front page](../README.md)

# On Mathematical Notation

Many textbooks on linear algebra use the convention of column vectors,
therefore this work uses column vectors too. Here, bold uppercase letters
(e.g. $`\boldsymbol{A}`$) are used for matrices, and bold lowercase letters
(e.g. $`\boldsymbol{x}`$) are used for vectors. Normal lowercase letters
are scalars (e.g. $`x`$).

A column vector $`\boldsymbol{x}`$ can defined by its elements in various
ways, including with transpose $`{}^\mathrm{T}`$ to typeset it on a single
line:
```math
\boldsymbol{x} =
\begin{bmatrix}
	x_1 \\
	\vdots \\
	x_K
\end{bmatrix} =
[ x_1, \ldots, x_K ]^\mathrm{T}
\quad \textrm{or} \quad
\boldsymbol{x}^\mathrm{T} =[ x_1, \ldots, x_K ] .
```

An N-by-M matrix $`\boldsymbol{A}`$ can be defined by its row and column
indexed elements as in
```math
\boldsymbol{A} =
\begin{bmatrix}
	a_{1,1} & \ldots & a_{1,M} \\
	\vdots & \ddots & \vdots \\
	a_{N,1} & \ldots & a_{N,M}
\end{bmatrix} .
```

Let
```math
\boldsymbol{A}\boldsymbol{x} = \boldsymbol{b}
```
be a set of linear equations. In this order of multiplication
$`\boldsymbol{x}`$ and $`\boldsymbol{b}`$ are necessarily column vectors,
otherwise the multiplication is meaningless as the dimensions (the number of
columns in $`\boldsymbol{A}`$ and the number of rows in $`\boldsymbol{x}`$)
do not match.

If you are accustomed to working with row vectors, you have to apply
transpose to the equation
```math
(\boldsymbol{A}\boldsymbol{x})^\mathrm{T} = \boldsymbol{b}^\mathrm{T}
```
which will give you the more familiar order of multiplication
```math
\boldsymbol{x}^\mathrm{T}\boldsymbol{A}^\mathrm{T} = \boldsymbol{b}^\mathrm{T}
```
where $`\boldsymbol{x}^\mathrm{T}`$ is a row vector.


## Projective 4x4 Transformation Matrices

Let $`\boldsymbol{M}`$ be a four-by-four projective transformation matrix
and $`\boldsymbol{x}`$ and $`\boldsymbol{x}'`$ be four-vectors
```math
\boldsymbol{M} = \begin{bmatrix}
	\sdot & \sdot & \sdot & t_\mathrm{x} \\
	\sdot & \sdot & \sdot & t_\mathrm{y} \\
	\sdot & \sdot & \sdot & t_\mathrm{z} \\
	p_\mathrm{x} & p_\mathrm{y} & p_\mathrm{z} & 1
\end{bmatrix}, \quad
\boldsymbol{x} = \begin{bmatrix}
	x \\
	y \\
	z \\
	1
\end{bmatrix}, \quad
\boldsymbol{x}' = \begin{bmatrix}
	x' \\
	y' \\
	z' \\
	w'
\end{bmatrix} .
```
Here $`t_\mathrm{x}, t_\mathrm{y}, t_\mathrm{z}`$ are the translation
component of the transformation, and
$`p_\mathrm{x}, p_\mathrm{y}, p_\mathrm{z}`$ are the projective component
(zeros for an affine transformation). The other matrix elements have been
left unwritten for clarity.

Let $`\boldsymbol{x}'`$ be the image of $`\boldsymbol{x}`$ through the
transformation $`\boldsymbol{M}`$, then
```math
\boldsymbol{x}' = \begin{bmatrix}
	x' \\
	y' \\
	z' \\
	w'
\end{bmatrix}
= \begin{bmatrix}
	\sdot & \sdot & \sdot & t_\mathrm{x} \\
	\sdot & \sdot & \sdot & t_\mathrm{y} \\
	\sdot & \sdot & \sdot & t_\mathrm{z} \\
	p_\mathrm{x} & p_\mathrm{y} & p_\mathrm{z} & 1
\end{bmatrix}\begin{bmatrix}
	x \\
	y \\
	z \\
	1
\end{bmatrix}
= \boldsymbol{M}\boldsymbol{x} .
```
Here you can see how the numbers of rows and columns need to agree, and
how the translation component indeed multiplies with $`1`$ to be added to
the input vector. If the projective component is zeros, you see that
$`w' = 1`$ as well.

Again, if you are accustomed to working with row vectors, you may be more
familiar with projective transformation matrices of the form
```math
\begin{bmatrix}
	\sdot & \sdot & \sdot & p_\mathrm{x} \\
	\sdot & \sdot & \sdot & p_\mathrm{y} \\
	\sdot & \sdot & \sdot & p_\mathrm{z} \\
	t_\mathrm{x} & t_\mathrm{y} & t_\mathrm{z} & 1
\end{bmatrix}
= \boldsymbol{M}^\mathrm{T}
```
where the translation is on the last row instead of the last column. This is
just a transpose of the more common notation in linear algebra.


## Row- and Column-major Matrix layout

All the above is only about the mathematical notation, which is to say that
row-major versus column-major matrix layout is irrelevant. In mathematical
notation matrices are two-dimensional arrays of numbers. The concept of row-
or column-major does not exist. Only when converting a matrix into a
one-dimensional array representation in computer memory one needs to decide
on a row- or column-major layout (or even a zig-zag layout like pixels in
JPEG).

Still, when programming computers to do matrix computations, the matrix
layout in memory is an important detail. If your computational algorithms
can cope with both row- and column-major layouts, taking the transpose of
a matrix becomes "free": you pretend the matrix is in the opposite layout
to produce the effect of transpose without needing to actually copy or
move values.

To avoid confusion, it is very important to keep the mathematical concepts
well separated from the computational details. Row vectors and column
vectors are mathematical concepts, while row-major and column-major are
computational details. One does not imply anything about the other.

Row- and column-major are not the only layouts widely used. Sparse matrices,
triangular matrices, diagonal matrices, and decomposition results (e.g.
LU-decomposition) may use specific optimized layouts. A common theme is to
not explicitly store entries that are known to be zero or one.
