---
SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
SPDX-License-Identifier: CC-BY-4.0
---

# Contributing to fourbyfour

If you have a 4x4 matrix inversion implementation that you would like to
include in fourbyfour to be benchmarked, I would be happy to have it if it
has a compatible license. Using a Meson sub-project with a wrap file
is recommended (see how Graphene was
[integrated](https://gitlab.freedesktop.org/pq/fourbyfour/-/commit/2cff1d8bc95770a21b126270771bbdb005cc4cd3)).

If you think the testing methods here are inadequate or flawed, I am open to
merge requests to improve them.

Since this is a one-off hobby project, I cannot promise to review big or
complicated contributions in any timely manner.

## How to contribute

All contributions should come as Gitlab merge requests.

Gitlab CI will ensure some of the technicalities, including Signed-off-by
and proper license and copyright notices. CI will mostly run for merge
requests only.

The code added into this project must be under MIT license. Non-code may
be under CC-BY-4.0 license.

You must agree to the
[Developer Certificate of Origin, Version 1.1](README.d/DCO-1.1.txt)
and signify that by adding your Signed-off-by tag to each commit
message. The command ``git commit -s`` does it for you.

This project follows the
[Freedesktop.org Code of Conduct](https://www.freedesktop.org/wiki/CodeOfConduct/).

\- *Pekka Paalanen*
