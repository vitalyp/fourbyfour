/*
 * SPDX-FileCopyrightText: 2020 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <cblas.h>

#include "fourbyfour.h"

/** Matrix-matrix multiplication
 *
 * Computes M = A * B.
 */
FBF_EXPORT void
fbf_matrix_matrix_mul(struct fbf_matrix *M,
		      const struct fbf_matrix *A,
		      const struct fbf_matrix *B)
{
	fbf_matrix_zero(M);
	cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, 4, 4, 4,
		    1.0, A->v, 4, B->v, 4, 0.0, M->v, 4);
}

/** Matrix-diagonal-matrix multiplication
 *
 * Computes M = A * diag(D) * B.
 */
FBF_EXPORT void
fbf_matrix_diag_matrix_mul(struct fbf_matrix *M,
			   const struct fbf_matrix *A,
			   const struct fbf_vector *D,
			   const struct fbf_matrix *B)
{
	struct fbf_matrix T;
	int i;

	/* T = diag(D) * B */
	T = *B;
	for (i = 0; i < 4; i++) {
		cblas_dscal(4, D->v[i], &T.v[i], 4);
	}

	fbf_matrix_matrix_mul(M, A, &T);
}

/** Matrix element-wise difference
 *
 * Computes M = A - B.
 */
FBF_EXPORT void
fbf_matrix_diff(struct fbf_matrix *M,
		const struct fbf_matrix *A,
		const struct fbf_matrix *B)
{
	*M = *A;
	cblas_daxpy(16, -1.0, B->v, 1, M->v, 1);
}
