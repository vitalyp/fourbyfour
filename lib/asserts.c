/*
 * SPDX-FileCopyrightText: 2020-2021 Pekka Paalanen <pq@iki.fi>
 *
 * SPDX-License-Identifier: MIT
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "fourbyfour.h"

/** Assert that values are identical down to last bit */
FBF_EXPORT void
fbf_assert_eq_double(double x, double ref)
{
	if (x == ref)
		return;

	fprintf(stderr, "Assertion failure, expected %.16g, got %.16g.\n", ref, x);
	exit(1);
}

/** Assert that relative error is smaller than threshold
 *
 * Compares |ref - x| / |ref| <= eps. Passes if true.
 *
 * -log_10(eps) tells you how many digits need to be correct.
 */
FBF_EXPORT void
fbf_assert_eq_double_releps(double x, double ref, double eps)
{
	double reldiff = fabs(ref - x) / fabs(ref);
	if (reldiff <= eps)
		return;

	fprintf(stderr,
		"Assertion failure, expected %g, got %g.\n"
		"Relative difference %g exceeds threshold %g.\n"
		"Absolute difference %.16g.\n",
		ref, x, reldiff, eps, fabs(ref - x));
	exit(1);
}

/** Assert that vectors are close enough relative to infinity norm
 *
 * Compares |ref - x| / |ref| <= eps. Passes if true.
 */
FBF_EXPORT void
fbf_assert_eq_vector_releps(const struct fbf_vector *x,
			    const struct fbf_vector *ref, double eps)
{
	double ref_x_norm = 0.0;
	double ref_norm = 0.0;
	double tmp;
	int i;

	for (i = 0; i < 4; i++) {
		tmp = fabs(ref->v[i] - x->v[i]);
		if (tmp > ref_x_norm)
			ref_x_norm = tmp;

		tmp = fabs(ref->v[i]);
		if (tmp > ref_norm)
			ref_norm = tmp;
	}

	tmp = ref_x_norm / ref_norm;
	if (tmp <= eps)
		return;

	fprintf(stderr,
		"Assertion failure, relative vector error %g exceeds threshold %g (infinity norm).\n",
		tmp, eps);
	fprintf(stderr, "Expected vector: %12.8g %12.8g %12.8g %12.8g\n",
		ref->v[0], ref->v[1], ref->v[2], ref->v[3]);
	fprintf(stderr, "     Got vector: %12.8g %12.8g %12.8g %12.8g\n",
		x->v[0], x->v[1], x->v[2], x->v[3]);
	exit(1);
}

/** Assert that matrices are close enough relative to infinity norm
 *
 * Ensures fbf_matrix_rel_error_inf(A, A_ref) <= eps.
 */
FBF_EXPORT void
fbf_assert_eq_matrix_releps_inf(const struct fbf_matrix *A,
				const struct fbf_matrix *A_ref,
				double eps)
{
	double reldiff = fbf_matrix_rel_error_inf(A, A_ref);
	if (reldiff <= eps)
		return;

	fprintf(stderr,
		"Assertion failure, relative matrix error %g exceeds threshold %g (infinity norm).\n",
		reldiff, eps);
	fprintf(stderr, "Expected matrix:\n");
	fbf_matrix_print(stderr, A_ref);
	fprintf(stderr, "Got matrix:\n");
	fbf_matrix_print(stderr, A);

	exit(1);
}
